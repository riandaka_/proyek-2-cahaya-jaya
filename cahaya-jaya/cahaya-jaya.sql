-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2019 at 01:57 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cahaya-jaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `rekening` varchar(15) NOT NULL,
  `saldo` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `username`, `password`, `name`, `email`, `address`, `city`, `telp`, `rekening`, `saldo`, `image`) VALUES
(1, 'farhan', 'farhan', 'Farhan Maulana', 'farhanmaulana88@gmail.com', 'Jl.Pasir Impun, Gg Haji Mulya No.54 RT.03 RW.12', 'Bandung', '081321319954', '7364810294', 17123000, 'farhan.jpg'),
(3, 'fadli', 'fadli', 'Fadli Wirahmasila K', 'fadliwk@gmail.com', 'No.830 C, Jl. Jendral Ahmad Yani No.830 C, Cicaheum, Kiaracondong, Bandung City, West Java 40272', 'Bandung', '087463781922', '6354920916', 1000000, 'fadli.jpg'),
(4, 'rizal', 'rizal', 'Riandaka Rizal', 'rizal@gmail.com', 'Muhammad Toha No.22', 'Bandung', '089864678754', '2147365982', 7000000, 'rizal1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categori_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categori_id`, `category`) VALUES
(1, 'Fashion'),
(2, 'Electronic'),
(3, 'Accessories'),
(4, 'Cosmetics'),
(5, 'Antiques'),
(6, 'Household Appliance');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `account_id`, `product_id`, `comment`, `date`) VALUES
(1, 4, 4, 'adaw', '2018-12-29 16:12:18'),
(2, 1, 4, 'qwe', '2018-12-29 16:15:13'),
(3, 1, 1, 'qwe', '2019-01-03 00:33:33'),
(4, 3, 1, 'dsa', '2019-01-03 00:35:18'),
(5, 3, 4, 'dsaf', '2019-01-03 00:46:09'),
(6, 1, 4, 'kjsnjsadkjn', '2019-01-03 14:27:44'),
(7, 1, 4, 'aaaaaaaaa', '2019-01-03 15:05:38'),
(8, 1, 4, 'qw', '2019-01-03 15:07:25'),
(9, 1, 4, 'afd', '2019-01-03 15:09:01'),
(10, 1, 4, 'jo', '2019-01-03 15:12:37'),
(11, 1, 4, 'a', '2019-01-03 15:13:37'),
(12, 1, 4, 'te', '2019-01-03 15:14:00'),
(13, 1, 4, 'ty', '2019-01-03 15:19:24'),
(14, 4, 13, 'sangat bagus', '2019-01-05 14:21:40'),
(15, 1, 4, 'mantap', '2019-01-05 14:38:02'),
(16, 1, 5, 'asd', '2019-01-10 12:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `invoice_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_status` enum('Paid','In Progress','Cancelled') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`invoice_id`, `buyer_id`, `price`, `date`, `payment_status`) VALUES
(43, 3, 30000000, '2018-12-31 16:50:03', 'In Progress'),
(44, 3, 1250000, '2018-12-31 16:56:59', 'In Progress'),
(45, 3, 15000000, '2018-12-31 17:07:19', 'In Progress'),
(46, 1, 1250000, '2019-01-02 13:03:58', 'Paid'),
(47, 1, 1000000, '2019-01-02 16:26:35', 'Paid'),
(48, 1, 500000, '2019-01-02 17:13:50', 'Paid'),
(49, 1, 1500000, '2019-01-02 21:19:22', 'Paid'),
(50, 1, 750000, '2019-01-02 22:13:40', 'Paid'),
(51, 1, 150000000, '2019-01-03 14:02:04', 'In Progress'),
(52, 1, 750000, '2019-01-03 15:36:43', 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `shipping_status` enum('On Progress','On Delivery','Already Received') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `invoice_id`, `buyer_id`, `seller_id`, `product_id`, `price`, `quantity`, `shipping_status`) VALUES
(35, 43, 1, 0, 1, 30000000, 2, ''),
(36, 44, 3, 0, 5, 1250000, 5, 'On Delivery'),
(37, 45, 3, 0, 1, 15000000, 1, 'On Progress'),
(38, 46, 1, 3, 4, 1250000, 5, 'Already Received'),
(39, 47, 1, 3, 4, 500000, 2, 'Already Received'),
(40, 47, 1, 3, 5, 500000, 2, 'Already Received'),
(41, 48, 1, 3, 5, 500000, 2, 'Already Received'),
(42, 49, 1, 3, 5, 1000000, 4, 'Already Received'),
(43, 49, 1, 3, 4, 500000, 2, 'Already Received'),
(44, 50, 1, 3, 4, 500000, 2, 'Already Received'),
(45, 50, 1, 3, 5, 250000, 1, 'Already Received'),
(46, 51, 1, 1, 1, 150000000, 10, 'On Progress'),
(47, 52, 1, 3, 5, 750000, 3, 'On Progress');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `categori_id` int(11) NOT NULL,
  `product_name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `product_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `account_id`, `categori_id`, `product_name`, `description`, `price`, `stock`, `product_image`) VALUES
(1, 1, 1, 'Blus', 'BLUS are generally worn by women. Blouses are also made of cotton or silk or if the names are better known are collars and arms. Clothes Blouses also have feminine details such as ties, ribbons on the neck, folds, emboroid or decorative buttons. Clothes This blouse is indeed standard, usually used to go to the office.', 100000, 4, '1.jpg'),
(4, 3, 1, 'Cardigan', 'CARDIGAN is a kind of jacket that opens on the front and can be given variations with buttons or zippers. The name cardigan comes from James Thomas Brudenell, Earl of Cardigan, an English military commander. This cardigan can be made using a machine or manual from wool or cotton.', 125000, 3, '7.jpg'),
(5, 3, 1, 'Suit', 'Navy suits (navy) are a classic choice that is timeless in making men look more stylish. Choose a two-breasted suit with a variety of pocket options throughout the front of the clothing, both inside and outside. Do not forget to complete the suit with a small stripe motif shirt that is calm, like a white alloy and a light blue line for example. Don\'t forget to add a neat fold of handkerchief to the breast pocket of the coat you are wearing.', 485000, 7, '8.jpg'),
(7, 1, 1, 'Pencil Skirt', 'This PENCIL SKIRT skirt makes your appearance look very formal. Combined with work shirts is also okay. Even with a more relaxed boss still makes this skirt give a formal impression. PLILD SKIRT is work clothes that are suitable for various events.', 85000, 6, '2.jpg'),
(8, 1, 1, 'Dress', ' DRESS if the Blouse only consists of superiors, while the dres consist of united superiors and subordinates. The word dress comes from English which means dress.', 150000, 10, '6.jpg'),
(9, 1, 1, 'Bolero', 'BOLERO is a kind of jacket that fits on the body with a half chest size and opens on the front, can be short or long sleeves. The word bolero comes from a Spanish dance that has a dramatic step and stop meaning.', 70000, 6, '3.jpg'),
(10, 1, 1, 'Blazer', 'BLAZER is a kind of jacket that is worn as casual clothing but still looks pretty neat. Blazer shaped like a suit with more relaxed pieces.', 200000, 3, '4.png'),
(11, 1, 1, 'PANTSUIT', 'PANTSUIT is a blazer-shaped top with matching pants or skirts that are made from the same. You can choose these clothes to attend office events, which are not required to wear uniforms, or meet clients for example at dinner hours. Sometimes you also need this clothes for friends to apply for work. Pantsuit has also become a trend after Hillary Clinton \'popularized\' this fashion item during the campaign.', 450000, 2, '5.jpg'),
(12, 3, 1, 'Skinny Tie', 'If the black tie is attached to the description of office workers\' clothes, then navy is a smart easy solution to make the formal look more stylish and luxurious. Choose one that has a slim shape (skinny tie) and is made of silk to present the look of a classy man.', 125000, 4, '9.jpg'),
(13, 3, 1, 'White Shirts', 'White shirts, both long and short sleeves, are fashion items that must be owned by men. Its clean and minimalist appearance can be combined with a variety of style choices and also suitable to be worn on many occasions, both formal and casual.', 90000, 3, '10.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `topup`
--

CREATE TABLE `topup` (
  `topup_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `uniq_key` varchar(6) NOT NULL,
  `nominal` varchar(20) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Used','Not Used') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topup`
--

INSERT INTO `topup` (`topup_id`, `account_id`, `uniq_key`, `nominal`, `date`, `status`) VALUES
(16, 1, '035678', '1000000', '2019-01-12 22:43:53', 'Used'),
(17, 1, '123789', '1000000', '2019-01-12 22:34:33', 'Used'),
(18, 1, '024579', '2500000', '2019-01-12 22:45:13', 'Used'),
(19, 1, '012489', '350000', '2019-01-12 22:45:24', 'Used'),
(20, 1, '023469', '23000', '2019-01-12 22:49:37', 'Used'),
(21, 3, '025789', '1000000', '2019-01-12 23:08:59', 'Used');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categori_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `topup`
--
ALTER TABLE `topup`
  ADD PRIMARY KEY (`topup_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `topup`
--
ALTER TABLE `topup`
  MODIFY `topup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
