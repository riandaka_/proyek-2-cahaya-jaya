<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="Cahaya Jaya Poltekpos">
  
  <meta name="author" content="Themefisher.com">

  <title>Cahaya Jaya</title>

  <!-- Mobile Specific Meta-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.png')?>" />
  
  <!-- Themefisher Icon font -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/themefisher-font/style.css')?>">
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/slick-carousel/slick/slick.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/slick-carousel/slick/slick-theme.css')?>"/>
  
  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">

</head>

<body id="body">

<!-- Start Top Header Bar -->
<section class="top-header">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-xs-12 col-sm-4">
        <div class="contact-number">
          <i class="tf-ion-ios-telephone"></i>
          <span>0129- 12323-123123</span>
        </div>
      </div>
      <div class="col-md-4 col-xs-12 col-sm-4">
        <!-- Site Logo -->
        <div class="logo text-center">
          <a href="index.html">
            <!-- replace logo here -->
            <svg width="270px" height="29px" viewBox="0 0 155 29" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" font-size="30" font-family="AustinBold, Austin" font-weight="bold">
                    <g id="Group" transform="translate(-108.000000, -297.000000)" fill="#000000">
                        <text id="AVIATO">
                            <tspan x="80.94" y="325">CAHAYA JAYA</tspan>
                        </text>
                    </g>
                </g>
            </svg>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-xs-12 col-sm-4">
      <!-- Cart -->
      <ul class="top-menu text-right list-inline">
            <li class="dropdown cart-nav dropdown-slide">
              <a href="<?php echo base_url(). 'buyer/home_buyer/cart'; ?>"><i class="tf-ion-android-cart"></i>Cart</a>
            </li><!-- / Cart -->

            <!-- Search -->
            <li class="dropdown search dropdown-slide">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="tf-ion-ios-search-strong"></i> Search</a>
              <ul class="dropdown-menu search-dropdown">
                <li><form action="<?php echo base_url(). 'buyer/home_buyer/shop'; ?>" method="get"><input type="search" name="search" class="form-control" id="search" placeholder="Search..."></form></li>
              </ul>
            </li>
            <!-- / Search -->

            <li>
              <a href="<?php echo base_url(). 'buyer/home_buyer/logout'; ?>"><i class="tf-ion-log-out"></i> Logout</a>
            </li>

          </ul><!-- / .nav .navbar-nav .navbar-right -->
      </div>
    </div>
  </div>
</section><!-- End Top Header Bar -->


<!-- Main Menu Section -->
<section class="menu">
  <nav class="navbar navigation">
      <div class="container">
        <div class="navbar-header">
          <h2 class="menu-title">Main Menu</h2>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

        </div><!-- / .navbar-header -->

        <!-- Navbar Links -->
        <div id="navbar" class="navbar-collapse collapse text-center">
          <ul class="nav navbar-nav">

            <!-- Home -->
            <li class="dropdown ">
              <a href="<?php echo base_url(). 'buyer/home_buyer'; ?>"><i class="tf-ion-home"></i>&nbspHome</a>
            </li><!-- / Home -->


            <!-- Elements -->
           <li>
              <a href="<?php echo base_url(). 'seller/home_seller'; ?>"><i class="tf-ion-pricetags"></i>&nbspManage Product</a>
            </li><!-- / Pages -->
            <!-- Elements -->

           <li class="dropdown dropdown-slide">
              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350" aria-haspopup="true" aria-expanded="false"><i class="tf-ion-archive"></i>&nbspOrders In<span class="tf-ion-ios-arrow-down"></span></a>
              <ul class="dropdown-menu">
          <li><a href="<?php echo base_url(). 'seller/home_seller/order_in'; ?>">On Progress Payment</a></li>
          <li><a href="<?php echo base_url(). 'seller/home_seller/order_in2'; ?>">On Delivery</a></li>
          <li><a href="<?php echo base_url(). 'seller/home_seller/order_in3'; ?>">Already Received</a></li>
          </ul>
          </li>

            <li class="dropdown dropdown-slide">
              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350" aria-haspopup="true" aria-expanded="false"><i class="tf-ion-cash"></i>&nbspTop Up<span class="tf-ion-ios-arrow-down"></span></a>
              <ul class="dropdown-menu">
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/voucher'; ?>">Topup And Check</a></li>
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/voucher2'; ?>">History</a></li>

          </ul><!-- / .nav .navbar-nav
          </div><!--/.navbar-collapse -->
      </div><!-- / .container -->
  </nav>
</section>

<section class="products section bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title text-center">
          <h2><i class="tf-ion-pricetags"></i>&nbspYour Product</h2>
        </div>
      </div>
 <table class="table">
  <thead>
    <tr>
      <th width="2%" scope="col">No.</th>
      <th width="10%" scope="col">Image</th>
      <th width="20%" scope="col">Product Name</th>
      <th width="35%" scope="col">Description</th>
      <th width="10%" scope="col">Category</th>
      <th width="10%" scope="col">Price</th>
      <th width="10%" scope="col">Stock</th>
      <th width="15%" scope="col">Action</th>
    </tr>
    <p align="right">
    <a href="<?php echo base_url('seller/home_seller/form_create_product')?>" class ='btn btn-main' align="right">Create Product</a>
    </p>
    <br>
  </thead>

  <?php 
  $i=0;
  foreach ($product as $key) {
    $i++;
    ?>
  <tbody>
  
    <tr>
      <th scope="row"><?php echo $i ?></th>
      <td><img style="width:100px ;height:130px ;" src="<?php echo base_url() . 'assets/gambar_product/' .$key->product_image; ?>"></td>
      <td><?php echo $key->product_name ?></td>
      <td><textarea readonly style="width: 300px; height: 150px;"><?php echo $key->description ?></textarea></td>
      <td><?php echo $key->category ?></td>
      <td><?php echo $key->price ?></td>
      <td><?php echo $key->stock ?></td>
      <td>
      <span class="btn btn-sm btn-success" data-toggle="modal" data-target="#product-modal<?php echo $key->product_id;?>">Update</span>
      </td>
      <td>
      <a href="<?php echo base_url('seller/home_seller/delete_product/'.$key->product_id)?>" onclick="return validasi();" class ='btn btn-sm btn-danger'>Delete</a>
      </td>
    </tr>

    <!-- Modal -->
    <div class="modal product-modal fade" id="product-modal<?php echo $key->product_id;?>">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="tf-ion-close"></i>
      </button>
        <div class="modal-dialog " role="document">
          <div class="modal-content">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="modal-image">
                      <img class="img-responsive" src="<?php echo base_url().'/assets/gambar_product/'.$key->product_image ?>" style="height:420px;width:350px" alt="product-img" />
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="product-short-details">
                      
                    <form action="<?php echo base_url(). 'seller/home_seller/update_product'; ?>" method="post" enctype="multipart/form-data">

                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="product_id" value="<?php echo $key->product_id?>">
                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="product_image2" value="<?php echo $key->product_image?>">

                    <div class="form-group">
                    <span>Product Name</span>
                    <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="product_name" value="<?php echo $key->product_name?>">
                    </div>

                    <div class="form-group">
                    <span>Description</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="description" value="<?php echo $key->description?>">
                    </div>

                    <div class="form-group">
                    <span>Category</span>
                        <select name="categori_id" required class="form-control">
                          <option selected value="<?php echo $key->categori_id?>" required><?php echo $key->category?></option>
                          <?php foreach ($category as $c) { ?>
                          <option value="<?php echo $c->categori_id?>"><?php echo $c->category?></option>
                          <?php } ?>
                        </select>
                    <!-- <input style="font-size: 15px" type="text" class="form-control" name="categori_id" value="<?php echo $key->categori_id?>"> -->
                    </div>


                    <div class="form-group">
                    <span>Price</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="price" value="<?php echo $key->price?>">
                    </div>

                    <div class="form-group">
                    <span>Stock</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="stock" value="<?php echo $key->stock?>">
                    </div>

                    <div class="form-group">
                    <span>Product Image</span>
                    <input type="file" class="form-control" name="product_image">
                    </div>

                      &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                      <button type="submit" class="btn btn-main">Edit</button>
                      </form>

                      <!-- <a href="product-single.html" class="btn btn-transparent">View Product Details</a> -->
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
   
  </tbody>

  <!-- Modal -->
    

   <?php } ?>
   
</table>

   
      

</div>
</div>
</section>

<!-- Modal -->
    
    

<!-- =========================================================================================================================>> -->

<!--
Start Call To Action
==================================== -->

<section class="section instagram-feed">
  <div class="container">
    <div class="row">
      <div class="title">
        <h2>View us on instagram</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="instafeed"></div>
      </div>
    </div>
  </div>
</section>

<footer class="footer section text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="social-media">
          <li>
            <a href="">
              <i class="tf-ion-social-facebook"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="tf-ion-social-instagram"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="tf-ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="tf-ion-social-pinterest"></i>
            </a>
          </li>
        </ul>
        <ul class="footer-menu">
          <li>
            <a href="">CONTACT</a>
          </li>
          <li>
            <a href="">SHIPPING</a>
          </li>
          <li>
            <a href="">TERMS OF SERVICE</a>
          </li>
          <li>
            <a href="">PRIVACY POLICY</a>
          </li>
        </ul>
        <p class="copyright-text">Powered by Bootstrap</p>
      </div>
    </div>
  </div>
</footer>


    <!-- 
    Essential Scripts
    =====================================-->
    

    <!-- Main jQuery -->
    <script src="https://code.jquery.com/jquery-git.min.js"></script>
    <!-- Bootstrap 3.1 -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
    <!-- Instagram Feed Js -->
    <script src="<?php echo base_url('assets/plugins/instafeed.js/instafeed.min.js')?>"></script>
    <!-- Slick Carousel -->
    <script src="<?php echo base_url('assets/plugins/slick-carousel/slick/slick.min.js')?>"></script>
    <!-- Google Map js -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBItRd4sQ_aXlQG_fvEzsxvuYyaWnJKETk&callback=initMap"></script>

    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/jquery/dist/jquery.js')?>"></script>


    <!-- Main Js File -->
    <script src="<?php echo base_url('assets/js/script.js')?>"></script>

    <script type="text/javascript">
        function validasi() {
            var msg= confirm("Are you sure you want to delete this?");
            if (msg){
              return true ;
            }else{
              return false ;
            }
        }
    </script>

        
    


  </body>
  </html>