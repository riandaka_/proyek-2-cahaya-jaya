<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="Cahaya Jaya Poltekpos">
  
  <meta name="author" content="Themefisher.com">

  <title>Cahaya Jaya</title>

  <!-- Mobile Specific Meta-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.png')?>" />
  
  <!-- Themefisher Icon font -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/themefisher-font/style.css')?>">
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/slick-carousel/slick/slick.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/slick-carousel/slick/slick-theme.css')?>"/>
  
  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">

</head>

<body id="body">

<!-- Start Top Header Bar -->
<section class="top-header">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-xs-12 col-sm-4">
        <div class="contact-number">
          <i class="tf-ion-ios-telephone"></i>
          <span>0129- 12323-123123</span> <br>
        </div>

      </div>
      <div class="col-md-4 col-xs-12 col-sm-4">
        <!-- Site Logo -->
        <div class="logo text-center">
          <a href="index.html">
            <!-- replace logo here -->
            <svg width="270px" height="29px" viewBox="0 0 155 29" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" font-size="30" font-family="AustinBold, Austin" font-weight="bold">
                    <g id="Group" transform="translate(-108.000000, -297.000000)" fill="#000000">
                        <text id="AVIATO">
                            <tspan x="80.94" y="325">CAHAYA JAYA</tspan>
                        </text>
                    </g>
                </g>
            </svg>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-xs-12 col-sm-4">
      <!-- Cart -->
      <ul class="top-menu text-right list-inline">
            <li class="dropdown cart-nav dropdown-slide">
              <a href="<?php echo base_url(). 'buyer/home_buyer/cart'; ?>"><i class="tf-ion-android-cart"></i>Cart</a>
            </li><!-- / Cart -->

            <!-- Search -->
            <li class="dropdown search dropdown-slide">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="tf-ion-ios-search-strong"></i> Search</a>
              <ul class="dropdown-menu search-dropdown">
                <li><form action="<?php echo base_url(). 'buyer/home_buyer/shop'; ?>" method="get"><input type="search" name="search" class="form-control" id="search" placeholder="Search..."></form></li>
              </ul>
            </li>
            <!-- / Search -->

             <li>
              <a href="<?php echo base_url(). 'buyer/home_buyer/logout'; ?>"><i class="tf-ion-log-out"></i> Logout</a>
            </li><!-- / logout -->

          </ul><!-- / .nav .navbar-nav .navbar-right -->
      </div>
    </div>
  </div>
</section><!-- End Top Header Bar -->


<!-- Main Menu Section -->
<section class="menu">
  <nav class="navbar navigation">
      <div class="container">
        <div class="navbar-header">
          <h2 class="menu-title">Main Menu</h2>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

        </div><!-- / .navbar-header -->

        <!-- Navbar Links -->
        <div id="navbar" class="navbar-collapse collapse text-center">
          <ul class="nav navbar-nav">

            <!-- Home -->
            <li class="dropdown ">
              <a href="<?php echo base_url(). 'buyer/home_buyer'; ?>"><i class="tf-ion-home"></i>&nbspHome</a>
            </li><!-- / Home -->


            <!-- Elements -->
            <li>
              <a href="<?php echo base_url(). 'buyer/home_buyer/shop'; ?>"><i class="tf-ion-bag"></i>&nbspShop</a>
            </li><!-- / Elements -->
            
            <!-- Elements -->
            <li class="dropdown dropdown-slide">
              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350" aria-haspopup="true" aria-expanded="false"><i class="tf-ion-ios-briefcase"></i>&nbspShopping History<span class="tf-ion-ios-arrow-down"></span></a>
              <ul class="dropdown-menu">
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/shopping_history2'; ?>">On Progress</a></li>
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/shopping_history'; ?>">History</a></li>
              </ul>
            </li><!-- / Blog -->

            <li class="dropdown dropdown-slide">
              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350" aria-haspopup="true" aria-expanded="false"><i class="tf-ion-ios-copy"></i>&nbspInvoice History<span class="tf-ion-ios-arrow-down"></span></a>
              <ul class="dropdown-menu">
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/invoice_history2'; ?>">In Progress</a></li>
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/invoice_history'; ?>">History</a></li>
              </ul>
            </li>

            <!-- Pages -->
            <li>
              <a href="<?php echo base_url(). 'buyer/home_buyer/account'; ?>"><i class="tf-ion-ios-person"></i>&nbspAccount</a>
            </li><!-- / Pages -->

            <li>
              <a href="<?php echo base_url(). 'seller/home_seller'; ?>"><i class="tf-ion-android-playstore"></i>&nbspManage My Store!</a>
            </li><!-- / Pages -->

            <li class="dropdown dropdown-slide">
              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350" aria-haspopup="true" aria-expanded="false"><i class="tf-ion-cash"></i>&nbspTop Up<span class="tf-ion-ios-arrow-down"></span></a>
              <ul class="dropdown-menu">
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/voucher'; ?>">Topup And Check</a></li>
          <li><a href="<?php echo base_url(). 'buyer/home_buyer/voucher2'; ?>">History</a></li>
              </ul>
            </li>

          </ul><!-- / .nav .navbar-nav
          </div><!--/.navbar-collapse -->
      </div><!-- / .container -->
  </nav>
</section>


<section class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="content">
          <h1 class="page-name">Account</h1>
        </div>
      </div>
    </div>
  </div>
</section>




<section class="page-wrapper">
  <div class="contact-section">
    <div class="container">
      <div class="row">
        
        <!-- Contact Details -->
        
        <div class="contact-details col-md-6 " >
        <?php foreach ($account as $key) { ?>
          <div class="google-map">
            <img src="<?php echo base_url().'/assets/profile/'.$key->image ?>" style="width: 500px; height: 500px;">
          </div>
          <!-- Footer Social Links -->
          
          <!--/. End Footer Social Links -->
        </div>
        <!-- / End Contact Details -->

         <!-- Contact Form -->
        <div class="contact-details col-md-6 " >
        <ul class="contact-short-info" >
            <li>
              <i class="tf-ion-edit" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Name: <?php echo $key->name ?></span>
            </li>
            <li>
              <i class="tf-ion-person" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Username: <?php echo $key->username ?></span>
            </li>
            <li>
              <i class="tf-ion-key" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Passord: *****</span>
            </li>
            <li>
              <i class="tf-ion-android-mail" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Email: <?php echo $key->email ?></span>
            </li>
            <li>
              <i class="tf-ion-ios-home" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Address: <?php echo $key->address ?></span>
            </li>
            <li>
              <i class="tf-ion-map" style="font-size: 30px"></i>
              <span style="font-size: 20px">  City: <?php echo $key->city ?></span>
            </li>
            <li>
              <i class="tf-ion-android-phone-portrait" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Phone: <?php echo $key->telp ?></span>
            </li>

            <li>
              <i class="tf-ion-card" style="font-size: 30px"></i>
              <span style="font-size: 20px">  Rekening: <?php echo $key->rekening ?></span>
            </li>

            <li>
              <i class="tf-ion-cash" style="font-size: 30px"></i>
              <span style="font-size: 20px"><?= 'Rp. '.number_format($key->saldo,0,',','.'); ?></span>
            </li>
            <br>
            <li>
            <span data-toggle="modal" data-target="#product-modal<?php echo $key->account_id?>" class="btn btn-main">Edit Profile</span>
            </li>

          </ul>

          </div> 
          <!-- Modal -->
    <div class="modal product-modal fade" id="product-modal<?php echo $key->account_id;?>">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="tf-ion-close"></i>
      </button>
        <div class="modal-dialog " role="document">
          <div class="modal-content">
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="modal-image">
                      <img class="img-responsive" src="<?php echo base_url().'/assets/profile/'.$key->image ?>" style="height:600px;width:600px" alt="product-img" />
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="product-short-details">

                    <form action="<?php echo base_url(). 'buyer/home_buyer/update_account'; ?>" method="post" enctype="multipart/form-data">
                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="id" value="<?php echo $key->account_id?>">
                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="image2" value="<?php echo $key->image?>">
                    <div class="form-group">
                    <span>Name</span>
                    <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="name" value="<?php echo $key->name?>">
                    </div>

                    <div class="form-group">
                    <span>Username</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="username" value="<?php echo $key->username?>">
                    </div>

                    <div class="form-group">
                    <span>Password</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="password" value="<?php echo $key->password?>">
                    </div>

                    <div class="form-group">
                    <span>Email</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="email" value="<?php echo $key->email?>">
                    </div>

                    <div class="form-group">
                    <span>Address</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="address" value="<?php echo $key->address?>">
                    </div>

                    <div class="form-group">
                    <span>City</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="city" value="<?php echo $key->city?>">
                    </div>

                    <div class="form-group">
                    <span>Number</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="telp" value="<?php echo $key->telp?>">
                    </div>

                    <div class="form-group">
                    <span>Rekening</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="rekening" value="<?php echo $key->rekening?>">
                    </div>

                    <div class="form-group">
                    <span>Photo</span>
                    <input type="file" class="form-control" name="image">
                    </div>

                      &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                      <button type="submit" onclick="return validasi();" class="btn btn-main">Edit</button>
                      </form>
                      <!-- <a href="product-single.html" class="btn btn-transparent">View Product Details</a> -->
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>

          <?php } ?>

        <!-- <h2> Edit Your Account </h2> -->
<!--         <center>
         <h2 class="page-name">Change Data Account</h2>
         </center>
          <form id="contact-form" method="post" action="" role="form">
          
            <div class="form-group">
              <input type="text" placeholder="Username" class="form-control" name="username">
            </div>

            <div class="form-group">
              <input type="text" placeholder="Password" class="form-control" name="password">
            </div>

            <div class="form-group">
              <input type="text" placeholder="Name" class="form-control" name="name">
            </div>
            
            <div class="form-group">
              <input type="email" placeholder="Email" class="form-control" name="email">
            </div>
            
            <div class="form-group">
              <textarea rows="6" placeholder="Address" class="form-control" name="address" ></textarea> 
            </div>

            <div class="form-group">
              <input type="text" placeholder="City" class="form-control" name="city">
            </div>

            <div class="form-group">
              <input type="text" placeholder="Telp" class="form-control" name="telp">
            </div>
            
            <div id="cf-submit">
              <input type="submit" id="contact-submit" class="btn btn-transparent" value="Submit">
            </div>            
            
          </form>
        </div> -->
        <!-- ./End Contact Form -->
          
        
      
      </div> <!-- end row -->
    </div> <!-- end container -->
  </div>
</section>





<!-- Start Call To Action
==================================== --> -->

<section class="section instagram-feed">
  <div class="container">
    <div class="row">
      <div class="title">
        <h2>View us on instagram</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="instafeed"></div>
      </div>
    </div>
  </div>
</section>

<!-- ==========================================================================>> -->

<footer class="footer section text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="social-media">
          <li>
            <a href="">
              <i class="tf-ion-social-facebook"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="tf-ion-social-instagram"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="tf-ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="tf-ion-social-pinterest"></i>
            </a>
          </li>
        </ul>
        <ul class="footer-menu">
          <li>
            <a href="">CONTACT</a>
          </li>
          <li>
            <a href="">SHIPPING</a>
          </li>
          <li>
            <a href="">TERMS OF SERVICE</a>
          </li>
          <li>
            <a href="">PRIVACY POLICY</a>
          </li>
        </ul>
        <p class="copyright-text">Powered by Bootstrap</p>
      </div>
    </div>
  </div>
</footer>


    <!-- 
    Essential Scripts
    =====================================-->
    

    <!-- Main jQuery -->
    <script src="https://code.jquery.com/jquery-git.min.js"></script>
    <!-- Bootstrap 3.1 -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
    <!-- Instagram Feed Js -->
    <script src="<?php echo base_url('assets/plugins/instafeed.js/instafeed.min.js')?>"></script>
    <!-- Slick Carousel -->
    <script src="<?php echo base_url('assets/plugins/slick-carousel/slick/slick.min.js')?>"></script>
    <!-- Google Map js -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBItRd4sQ_aXlQG_fvEzsxvuYyaWnJKETk&callback=initMap"></script>

    <!-- Main Js File -->
    <script src="<?php echo base_url('assets/js/script.js')?>"></script>

    <script type="text/javascript">
        function validasi() {
            var msg= confirm("Are you sure you want update this data?");
            if (msg){
              return true ;
            }else{
              return false ;
            }
        }
    </script>
    


  </body>
  </html>